﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.UIElements;

namespace Throwmap
{
    [RequireComponent(typeof(Camera))]
    public class ThrowmapCamera : MonoBehaviour
    {
        Camera _cameraRef;

        /// <summary>
        /// Lazy init reference to camera.
        /// </summary>
        Camera Camera
        {
            get
            {
                if (!_cameraRef) _cameraRef = GetComponent<Camera>();
                return _cameraRef;
            }
        }

#if UNITY_EDITOR
        Display screen => Display.main;
#else
                Display screen => Display.displays[Camera.targetDisplay];

#endif

        public bool Active;
        public bool DrawLines;

        static Material lineMaterial;

        #region Inputs

        public BehaviorSubject<Vector2> RelativePoint = new BehaviorSubject<Vector2>(Vector2.zero);
        public ISubject<Unit> Trigger = new Subject<Unit>();

        IObservable<Vector3> Coords =>
            RelativePoint.Select(ConvertRelativePoint);

        #endregion

        #region Outputs

        readonly ISubject<Ray> _rays = new Subject<Ray>();
        public IObservable<Ray> Rays => _rays;

        #endregion

        void Awake()
        {
            ActivateDisplays();
        }

        static void CreateLineMaterial()
        {
            if (!lineMaterial)
            {
                // Unity has a built-in shader that is useful for drawing
                // simple colored things.
                Shader shader = Shader.Find("Hidden/Internal-Colored");
                lineMaterial = new Material(shader);
                lineMaterial.hideFlags = HideFlags.HideAndDontSave;
                // Turn on alpha blending
                lineMaterial.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.SrcAlpha);
                lineMaterial.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                // Turn backface culling off
                lineMaterial.SetInt("_Cull", (int) UnityEngine.Rendering.CullMode.Off);
                // Turn off depth writes
                lineMaterial.SetInt("_ZWrite", 0);
            }
        }

        void Start()
        {
            Trigger
                .WithLatestFrom(Coords, (_, p) => p)
                .Select(Raycast)
                .Subscribe(_rays)
                .AddTo(this);
        }

        public void OnPostRender()
        {
            if (!DrawLines) return;

            CreateLineMaterial();
            Vector3 mousePos = ConvertRelativePoint(RelativePoint.Value);

            // Apply the line material
            lineMaterial.SetPass(0);

            float distance = 1;
            Vector3 top = ScreenToWorld(new Vector3(mousePos.x, 0, 0), distance);
            Vector3 bottom = ScreenToWorld(new Vector3(mousePos.x, screen.renderingHeight, 0), distance);
            Vector3 left = ScreenToWorld(new Vector3(0, mousePos.y, 0), distance);
            Vector3 right = ScreenToWorld(new Vector3(screen.renderingWidth, mousePos.y, 0), distance);

            // Draw lines
            GL.Begin(GL.LINES);
            GL.Color(new Color(1, 1, 1, 0.8F));
            GL.Vertex(top);
            GL.Vertex(bottom);

            GL.Color(new Color(1, 1, 1, 0.8F));
            GL.Vertex(left);
            GL.Vertex(right);
            GL.End();
        }

        Vector3 ScreenToWorld(Vector3 screenPoint, float distance)
        {
            Ray ray = Raycast(screenPoint);
            return ray.GetPoint(distance);
        }

        Ray Raycast(Vector3 screenPoint)
        {
            return Camera.ScreenPointToRay(screenPoint);
        }

        void ActivateDisplays()
        {
            foreach (Display display in Display.displays)
            {
                display.Activate();
            }
        }

        Vector3 ConvertRelativePoint(Vector2 p)
        {
            return new Vector3(p.x * screen.renderingWidth, p.y * screen.renderingHeight, 0);
        }
    }
}