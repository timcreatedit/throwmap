﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using UniRx;
using UnityEditor;
using UnityEngine;

namespace Throwmap
{
    public class Throwmapper : MonoBehaviour
    {
        [Header("Drag In:")]
        public ThrowmapCamera Camera;
        public ThrowmapMesh[] Meshes;

        [Header("Editing References")]
        public GameObject VertexHighlighter;
        public Material EditMaterial;
        
        #region Inputs

        [Header("Settings")]
        public BoolReactiveProperty Recording;
        public FloatReactiveProperty Distance;

        public readonly ISubject<Mapping> Mapping = new Subject<Mapping>();
        
        public ISubject<Unit> NextVertex = new Subject<Unit>();
        public ISubject<Unit> PreviousVertex = new Subject<Unit>();
        public ISubject<Unit> NextMesh = new Subject<Unit>();
        public ISubject<Unit> PreviousMesh = new Subject<Unit>();

        #endregion

        #region Outputs

        public IObservable<Mapping> CurrentMapping => _currentMapping.AsObservable();

        #endregion

        ReactiveProperty<Mapping> _currentMapping = new ReactiveProperty<Mapping>();

        readonly BehaviorSubject<int> _meshIndex = new BehaviorSubject<int>(0);
        readonly BehaviorSubject<int> _vertexIndex = new BehaviorSubject<int>(0);

        readonly Subject<Ray> _currentRay = new Subject<Ray>();
        

        IObservable<Vector3> _currentPoint => _currentRay
            .CombineLatest(Distance, (ray, d) => ray.GetPoint(d));
        
        void Awake()
        {
            Debug.Assert(Camera != null && Meshes != null);
            Meshes = Meshes.ToList().Where(m => m != null).ToArray();
        }

        void Start()
        {
            Recording
                .Where(v => v)
                .Subscribe(_ => StartRecording())
                .AddTo(this);

            Recording
                .Subscribe(v => Camera.Active = v)
                .AddTo(this);

            Recording
                .Where(v => !v)
                .Select(_ => _currentMapping.Value)
                .Subscribe(StopRecording)
                .AddTo(this);
            
            Camera.Rays
                .Where(_ => Recording.Value)
                .Subscribe(_currentRay.OnNext)
                .AddTo(this);

            Mapping
                .Subscribe(m => _currentMapping.Value = m)
                .AddTo(this);

            
            NextVertex
                .Where(_ => Recording.Value)
                .WithLatestFrom(_vertexIndex, (_, i) => i + 1)
                .Subscribe(_vertexIndex)
                .AddTo(this);
            
            PreviousVertex
                .Where(_ => Recording.Value)
                .WithLatestFrom(_vertexIndex, (_, i) => i - 1)
                .Subscribe(_vertexIndex)
                .AddTo(this);
            
            NextMesh
                .Where(_ => Recording.Value)
                .WithLatestFrom(_meshIndex, (_, i) => i + 1)
                .Subscribe(_meshIndex)
                .AddTo(this);
            
            PreviousMesh
                .Where(_ => Recording.Value)
                .WithLatestFrom(_meshIndex, (_, i) => i - 1)
                .Subscribe(_meshIndex)
                .AddTo(this);

            _currentPoint
                .Subscribe(UpdatePoint)
                .AddTo(this);

            _meshIndex
                .Where(_ => Recording.Value)
                .Do(SelectMesh)
                .Subscribe((_) => _vertexIndex.OnNext(0))
                .AddTo(this);

            _vertexIndex
                .Where(_ => Recording.Value)
                .Select(SelectVertex)
                .Subscribe(HighlightVertex)
                .AddTo(this);

            _currentMapping
                .Where(m => m != null)
                .Subscribe(ApplyMapping)
                .AddTo(this);
        }

        void StartRecording()
        {
            if (_currentMapping.Value == null) _currentMapping.Value = InitNewMapping();

            if (Meshes.Length > 0) _meshIndex.OnNext(0);
        }

        void UpdatePoint(Vector3 point)
        {
            int realMeshIndex = mod(_meshIndex.Value, Meshes.Length);
            var mesh = Meshes[realMeshIndex];
            var mapping = _currentMapping.Value;
            var mappingMesh = mapping.Meshes[realMeshIndex];
            
            var mappingVertexindex = mod(_vertexIndex.Value, mappingMesh.Vertices.Count);
            var vertexPos = mappingMesh.Vertices[mappingVertexindex];

            for (int i = 0; i < mappingMesh.Vertices.Count; i++)
            {
                Vector3 v = mappingMesh.Vertices[i];
                if (v == vertexPos)
                {
                    mappingMesh.Vertices[i]= mesh.transform.InverseTransformPoint(point);
                }
            }

            HighlightVertex(point);
            _currentMapping.SetValueAndForceNotify(mapping);
        }

        Vector3 SelectVertex(int vertexIndex)
        {
            int realMeshIndex = mod(_meshIndex.Value, Meshes.Length);
            var mesh = Meshes[realMeshIndex];
            int realVertexIndex = mod(vertexIndex, mesh.Vertices.Count);
            var point = mesh.transform.TransformPoint(mesh.Vertices[realVertexIndex]);
            var cameraPos = Camera.transform.position;
            _currentRay.OnNext(new Ray(cameraPos, point - cameraPos));
            Distance.Value = Vector3.Distance(point, cameraPos);
            return point;
        }

        void SelectMesh(int index)
        {
            int realIndex = mod(index,Meshes.Length);

            ThrowmapMesh selectedMesh =  Meshes[realIndex];
            selectedMesh?.Material.OnNext(EditMaterial);
            foreach (ThrowmapMesh throwmapMesh in Meshes.Where(m => m != selectedMesh))
            {
                throwmapMesh.Material.OnNext(null);
            }
        }

        void UnselectMeshes()
        {
            foreach (ThrowmapMesh throwmapMesh in Meshes)
            {
                throwmapMesh.Material.OnNext(null);
            }
        }

        void HighlightVertex(Vector3 pos)
        {
            if (!VertexHighlighter)
            {
                Debug.LogWarning("No Vertex Higlighter defined - Vertices won't be highlighted");
                return;
            }

            VertexHighlighter.SetActive(true);
            VertexHighlighter.transform.position = pos;
        }

        Mapping InitNewMapping()
        {
            Mapping m = new Mapping();
            foreach (ThrowmapMesh throwmapMesh in Meshes)
            {
                var mapping = new MappedMesh();
                mapping.Vertices = throwmapMesh.Vertices;
                m.Meshes.Add(mapping);
            }

            return m;
        }

        public bool MappingMatches(Mapping m)
        {
            if (m.Meshes.Count != Meshes.Length) return false;
            for (int index = 0; index < Meshes.Length; index++)
            {
                ThrowmapMesh throwmapMesh = Meshes[index];
                MappedMesh mappedMesh = m.Meshes[index];
                if (throwmapMesh.Vertices.Count != mappedMesh.Vertices.Count) return false;
            }
            return true;
        }
        
        void ApplyMapping(Mapping m)
        {
            if (m == null) return;
            for (int index = 0; index < Meshes.Length; index++)
            {
                ThrowmapMesh throwmapMesh = Meshes[index];
                throwmapMesh.Mapping.OnNext(m.Meshes[index]);
            }
        }

        void StopRecording(Mapping mapping)
        {
            UnselectMeshes();
            if (mapping == null) return;
            VertexHighlighter?.SetActive(false);
        }

        void OnApplicationQuit()
        {
            Recording.Value = false;
        }
        
        
        static int mod(int a, int n) {
            return ((a%n)+n) % n;
        }
    }
}