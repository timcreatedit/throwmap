﻿using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;

namespace Throwmap.Viewmodel
{
    public class MappingViewmodel : MonoBehaviour
    {
        [Header("Mapper")] public Throwmapper Throwmapper;

        [Header("UI")] public GameObject Panel;

        public Toggle RecordToggle;
        public Button SaveBtn;
        public Button SaveAsBtn;
        public Button OpenBtn;
        public TMP_Text Filename;

        [Header("Settings")] public BoolReactiveProperty PanelOpen;

        public float ScrollFactor = .1f;
        public KeyCode PanelKey = KeyCode.LeftCommand;
        public KeyCode RecordKey = KeyCode.R;
        public KeyCode OpenKey = KeyCode.O;
        public KeyCode SaveKey = KeyCode.S;
        public KeyCode VertexKey = KeyCode.Space;
        public KeyCode MeshKey = KeyCode.Tab;

        readonly MappingRepo _repo = new MappingRepo();

        void Start()
        {
            this.UpdateAsObservable()
                .Select(_ => Input.GetKey(PanelKey))
                .Subscribe(PanelOpen.SetValueAndForceNotify)
                .AddTo(this);

            PanelOpen
                .Subscribe(Panel.SetActive)
                .AddTo(this);

            PanelOpen
                .CombineLatest(Throwmapper.Recording.AsObservable(), (open, recording) => !open && recording)
                .Subscribe(draw => Throwmapper.Camera.DrawLines = draw)
                .AddTo(this);

            BindUi();
            BindMouse();
            BindKeys();
        }

        void BindUi()
        {
            RecordToggle
                .OnValueChangedAsObservable()
                .Subscribe(v => Throwmapper.Recording.Value = v)
                .AddTo(this);

            Throwmapper.CurrentMapping
                .Select(m => m != null)
                .Subscribe(v =>
                {
                    SaveBtn.interactable = v;
                    SaveAsBtn.interactable = v;
                })
                .AddTo(this);
            
            SaveBtn
                .OnClickAsObservable()
                .WithLatestFrom(Throwmapper.CurrentMapping, (_, mapping) => mapping)
                .Where(m => m != null)
                .Select(m => _repo.SaveMapping(m))
                .Select(s => s ? "" : "un")
                .Subscribe(s => Debug.Log($"Saving {s}successful!"))
                .AddTo(this);
            
            SaveAsBtn
                .OnClickAsObservable()
                .WithLatestFrom(Throwmapper.CurrentMapping, (_, mapping) => mapping)
                .Where(m => m != null)
                .Select(m => _repo.SaveMapping(m, true))
                .Select(s => s ? "" : "un")
                .Subscribe(s => Debug.Log($"Saving {s}successful!"))
                .AddTo(this);
            
            OpenBtn
                .OnClickAsObservable()
                .Select(_ => _repo.LoadMapping())
                .Where(CheckMappingMatch)
                .Subscribe(Throwmapper.Mapping)
                .AddTo(this);

            _repo.Filename
                .Select(n => n ?? "")
                .Subscribe(n => Filename.text = n)
                .AddTo(this);
        }

        void BindKeys()
        {
            this.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(OpenKey))
                .Where(_ => PanelOpen.Value)
                .Select(_ => _repo.LoadMapping())
                .Subscribe(Throwmapper.Mapping)
                .AddTo(this);

            this.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(SaveKey))
                .Where(_ => PanelOpen.Value)
                .WithLatestFrom(Throwmapper.CurrentMapping, (_, mapping) => mapping)
                .Where(m => m != null)
                .Select(m => _repo.SaveMapping(m))
                .Select(s => s ? "" : "un")
                .Subscribe(s => Debug.Log($"Saving {s}successful!"))
                .AddTo(this);
            
            this.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(RecordKey))
                .Where(_ => PanelOpen.Value)
                .Select(_ => !Throwmapper.Recording.Value)
                .Subscribe(v => RecordToggle.isOn = v)
                .AddTo(this);

            this.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(VertexKey))
                .Select(_ => Input.GetKey(KeyCode.LeftShift) ? Throwmapper.PreviousVertex : Throwmapper.NextVertex)
                .Subscribe(o => o.OnNext(Unit.Default))
                .AddTo(this);

            this.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(MeshKey))
                .Select(_ => Input.GetKey(KeyCode.LeftShift) ? Throwmapper.PreviousMesh : Throwmapper.NextMesh)
                .Subscribe(o => o.OnNext(Unit.Default))
                .AddTo(this);
        }

        void BindMouse()
        {
            this.UpdateAsObservable()
                .Where(_ => Input.GetMouseButton(0))
                .Where(_ => !PanelOpen.Value)
                .Subscribe(Throwmapper.Camera.Trigger)
                .AddTo(this);

            this.UpdateAsObservable()
                .Select(_ => GetRelativePoint())
                .Subscribe(Throwmapper.Camera.RelativePoint)
                .AddTo(this);

            this.UpdateAsObservable()
                .Select(_ => Input.mouseScrollDelta.y * ScrollFactor)
                .WithLatestFrom(Throwmapper.Distance, (scroll, distance) => distance + scroll)
                .Subscribe(v => Throwmapper.Distance.Value = v)
                .AddTo(this);
        }

        bool CheckMappingMatch(Mapping m)
        {
            if (Throwmapper.MappingMatches(m)) return true;
            Debug.LogError("Loaded Mapping invalid! Discarding!");
            _repo.ClearMapping();
            return false;
        }

        Vector2 GetRelativePoint()
        {
            return new Vector2(Input.mousePosition.x / Screen.width, Input.mousePosition.y / Screen.height);
        }
    }
}