using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace Throwmap
{
    [XmlRoot("Throwmap")]
    [XmlInclude(typeof(MappedMesh))]
    public class Mapping
    {
        [XmlArray("Meshes")]
        [XmlArrayItem("MappedMesh")]
        public List<MappedMesh> Meshes = new List<MappedMesh>();
    }

    [XmlType("MappedMesh")]
    public class MappedMesh
    {
        [XmlArray("Vertices")]
        [XmlArrayItem("Vertex")]
        public List<Vector3> Vertices = new List<Vector3>();
    }
}