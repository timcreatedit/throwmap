using System;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using SFB;
using UniRx;
using UnityEngine;

namespace Throwmap
{
    public class MappingRepo
    {
        readonly BehaviorSubject<FileInfo> _currentPath = new BehaviorSubject<FileInfo>(null);

        public IObservable<string> Filename => _currentPath.Select(p => p?.Name.Replace($"{p.Extension}", ""));

        public bool SaveMapping(Mapping mapping, bool forceNewPath = false)
        {
            if (_currentPath.Value == null || forceNewPath)
            {
                DirectoryInfo strAssets = new DirectoryInfo(Application.streamingAssetsPath);
                if (!strAssets.Exists) strAssets.Create();
                string path = StandaloneFileBrowser.SaveFilePanel("Save Mapping", Application.streamingAssetsPath,
                    "", "throwmap");
                if (string.IsNullOrEmpty(path)) return false;
                _currentPath.OnNext(new FileInfo(path));
            }
            try
            {
                XMLOp.Serialize(mapping, _currentPath.Value.FullName);
            }
            catch (Exception e)
            {
                throw new Exception("Unable to save!", e);
            }

            return true;
        }

        public Mapping LoadMapping(bool forceNewPath = true)
        {
            if (_currentPath.Value == null || forceNewPath)
            {
                DirectoryInfo strAssets = new DirectoryInfo(Application.streamingAssetsPath);
                if (!strAssets.Exists) strAssets.Create();
                string[] paths = StandaloneFileBrowser.OpenFilePanel("Open Mapping", Application.streamingAssetsPath,
                    "throwmap", false);
                if (string.IsNullOrEmpty(paths.FirstOrDefault())) return null;
                _currentPath.OnNext(new FileInfo(paths.FirstOrDefault()));
            }

            try
            {
                return XMLOp.Deserialize<Mapping>(_currentPath.Value.FullName);
            }
            catch (Exception e)
            {
                throw new Exception("Loading Error", e);
            }
        }

        public void ClearMapping()
        {
            _currentPath.OnNext(null);
        }
    }
}