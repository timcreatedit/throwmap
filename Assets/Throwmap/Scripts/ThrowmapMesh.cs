﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using Object = System.Object;

namespace Throwmap
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class ThrowmapMesh : MonoBehaviour
    {
        MeshFilter _meshFilterRef;
        MeshFilter MeshFilter
        {
            get
            {
                if (!_meshFilterRef) _meshFilterRef = GetComponent<MeshFilter>();
                return _meshFilterRef;
            }
        }


        MeshRenderer _meshRendererRef;
        MeshRenderer MeshRenderer
        {
            get
            {
                if (!_meshRendererRef) _meshRendererRef = GetComponent<MeshRenderer>();
                return _meshRendererRef;
            }
        }
        
        Material[] _originalMaterials;
        Mesh _originalMesh;
        Mesh _clonedMesh;

        #region Inputs

        public readonly ISubject<MappedMesh> Mapping = new Subject<MappedMesh>();
        public readonly ISubject<Material> Material = new Subject<Material>();

        #endregion

        #region Outputs


        public List<Vector3> Vertices => _clonedMesh.vertices.ToList();

        #endregion

        void Awake()
        {
            InitMesh();
        }

        void Start()
        {
            Mapping
                .Subscribe(ApplyMapping)
                .AddTo(this);

            Material
                .Subscribe(ApplyMaterial)
                .AddTo(this);
        }

        void InitMesh()
        {
            _originalMesh = MeshFilter.sharedMesh; //1
            _originalMaterials = MeshRenderer.materials;
            
            _clonedMesh = new Mesh(); //2

            _clonedMesh.name = "clone";
            _clonedMesh.vertices = _originalMesh.vertices;
            _clonedMesh.triangles = _originalMesh.triangles;
            _clonedMesh.normals = _originalMesh.normals;
            _clonedMesh.uv = _originalMesh.uv;
            MeshFilter.mesh = _clonedMesh;  //3
            Debug.Log("Init & Cloned");
        }

        void ApplyMaterial(Material m)
        {
            if (m == null)
            {
                MeshRenderer.materials = _originalMaterials;
                return;
            }

            MeshRenderer.materials = MeshRenderer.materials.Select(_ => m).ToArray();
        }

        void ApplyMapping(MappedMesh mapping)
        {
            if (mapping.Vertices.Count > _clonedMesh.vertices.Length) throw new Exception("Mapping not matching mesh");
            var verts = _clonedMesh.vertices;
            for (var i = 0; i < _clonedMesh.vertices.Length; i++)
            {
                if (mapping.Vertices.Count < i) continue;
                verts[i] = mapping.Vertices[i];
            }

            _clonedMesh.vertices = verts;
        }
        
    }
}
