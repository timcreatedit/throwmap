# Throwmap

A WIP Unity Asset that allows for easy projection mapping of any geometry. 
It works by distorting the objects vertex by vertex.

Look at the **Throwmap** folder for all relevant content.